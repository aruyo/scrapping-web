# README #

This project is using Puppeteer for scrapping the website of Tokopedia. We will scrap the data from Handphone Category. In this script, we will fetch the item's name, price, image URL, rating and description. After we fetch the data, we will store it in the csv file. The file will be generated when you run the script.

We also include some unit testing using Jest.

#### Tech stack ###

- NodeJS OOP (Object Oriented Programming)
- Puppeteer
- Jest

### How do I get set up? ###

- Assuming that you already has NodeJS installed in your device.
- Run the installation package with command `npm install`
- After the initial installation package success then run the command inside the project directory `node index.js` to process the scrapping
- The result of the scrapping are in the file `top-100-handphone.csv` with expecting output below the sample
- To run the test cases with command `npm test` or `npm run test`

** Expecting output for scrapping **

```
$ node index.js
	Items: 100
	Item: 1
	Item: 2
	Item: 3
	Item: 4
	Item: 5
	Item: 6
	Item: 7
	Item: 8
	Item: 9
	Item: 10
	Item: 11
	Item: 12
	Item: 13
	Item: 14
	Item: 15
	Item: 16
	Item: 17
	Item: 18
	Item: 19
	Item: 20
	Item: 21
	Item: 22
	Item: 23
	Item: 24
	Item: 25
	Item: 26
	Item: 27
	Item: 28
	Item: 29
	Item: 30
	Item: 31
	Item: 32
	Item: 33
	Item: 34
	Item: 35
	Item: 36
	Item: 37
	Item: 38
	Item: 39
	Item: 40
	Item: 41
	Item: 42
	Item: 43
	Item: 44
	Item: 45
	Item: 46
	Item: 47
	Item: 48
	Item: 49
	Item: 50
	Item: 51
	Item: 52
	Item: 53
	Item: 54
	Item: 55
	Item: 56
	Item: 57
	Item: 58
	Item: 59
	Item: 60
	Item: 61
	Item: 62
	Item: 63
	Item: 64
	Item: 65
	Item: 66
	Item: 67
	Item: 68
	Item: 69
	Item: 70
	Item: 71
	Item: 72
	Item: 73
	Item: 74
	Item: 75
	Item: 76
	Item: 77
	Item: 78
	Item: 79
	Item: 80
	Item: 81
	Item: 82
	Item: 83
	Item: 84
	Item: 85
	Item: 86
	Item: 87
	Item: 88
	Item: 89
	Item: 90
	Item: 91
	Item: 92
	Item: 93
	Item: 94
	Item: 95
	Item: 96
	Item: 97
	Item: 98
	Item: 99
	Item: 100
	... (Print all of the result into the terminal)
```

### Result ###

The `top-100-handphone.csv` file are containing with scrapping result

- Name of Product

- Description

- Image Link

- Price

- Rating(out of 5 stars)

** Expecting ouput for test cases **

```
$ npm test

> scrapping-web@1.0.0 test /Users/aryoprakarsa/Sites/scrapping-web
> jest

 PASS  __test__/testFetechedData.spec.js
  Validation function
    ✓ it should have valid data with 6 attributes on all 100 items (5 ms)

Test Suites: 1 passed, 1 total
Tests:       1 passed, 1 total
Snapshots:   0 total
Time:        2.694 s
Ran all test suites.
```
